# Newspaper app

## Desctiption

That's a Newspaper application, developed Django framework. At this project realized authorization, authentication. User are able to add new articles and comments to them. Additionally covered by unit tests

### Instalation

```bash
    pip install django
    pip install python-dotenv
```

Before start create .env file in root of the project and fill according to example.env

### Start project

```bash
    python manage.py runserver
```

Application starts at 8000 port by default

### Tests

```bash
    python manage.py test
```
